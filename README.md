# FNS 管理系统
---------------------------------

> FNS 管理后台是七桃在 2019 年推出的配合光合保使用的管理系统。
> 截止到 2020 年 7 月底，版本开发至 3.6.1。以下就 3.6.1 整理一下 FNS,讲解的顺序不一定按照菜单排列的先后顺序。

FNS 分为**系统管理、市场运营、保险规划、产品**四大模块，后面会一一讲到。（第五模块“报表”正在开发中）

*说明：文中提及的公司均指七桃科技。文中所有涉及的系统链接和系统截图均基于测试环境。*

### 登录

七桃内部员工可登录系统，用户名为员工名字全拼，初始密码为 `123456`。需要说明的是，员工首先要确保已被录入系统中（下面会说明录入系统），员工登录后可在右上角个人设置中重新设置自己的登录密码。

### 系统管理

每个后台的`系统管理`都是整个系统的重中之重，它一般涉及到整个系统的底层应用。我们的`系统管理`同样也是如此，它主要分为五个页面，分别是`员工管理`、`角色管理`、`规划师配置`、`日程底层数据`和`客户交接`。

![](https://gitlab.com/linhe/qtao-cloud-service/-/raw/master/qtao/system-manage.png)

##### 角色管理

- [页面地址](https://testfns.guanghebao.com/#/nav/system/roleManage)
- 角色：系统管理员

`角色管理`用于各角色权限的分配，一般主要分为管理员、市场运营、规划师等。其中管理员理所当然拥有系统最高权限，而市场运营则主要拥有`市场运营`模块权限，规划师主要拥有`保险规划`模块的**部分权限**。

在该页面下管理员可实现角色的增删改查，而且不仅限于创建上述几类角色。管理员可完全自定义角色名称和页面权限，以及部分页面的关键按钮权限等。

##### 员工管理

- [页面地址](https://testfns.guanghebao.com/#/nav/system/employeeManage)
- 角色：系统管理员

公司内部员工在入职后会被录入员工系统，配置职责对应的权限以及是否授权登录等。`员工管理`记录了但不限于员工的名字，手机号，入职时间等。

我们可以根据创建的角色来为员工分配对应权限。

##### 规划师配置

- [页面地址](https://testfns.guanghebao.com/#/nav/system/plannerManage)
- 角色：系统管理员

规划师配置页主要为规划师配置头像以及是否参加日程排班等。添加规划师的方法为输入员工的名字全拼，实际上就是从`员工管理`中匹配。

##### 日程底层数据

- [页面地址](https://testfns.guanghebao.com/#/nav/system/scheduleData)
- 角色：系统管理员

所有日程数据展示。

区分个人日程和客户日程，区别在于是否与客户 ID 绑定。

##### 客户交接

- [页面地址](https://testfns.guanghebao.com/#/nav/system/userHandover)
- 角色：系统管理员

客户交接指的是客户从 A 规划师的名下转到 B 规划师的名下。不仅仅包括未服务或服务中的客户，服务终止（已投保）的客户也能交接。交接方式是通过本地自建的 Excel 表提交数据，每次交接最多支持500位客户，交接后，被交接出去的客户对旧规划师不再可见。

### 市场运营

不言而喻，`市场运营`就是专为运营定制的模块，它包括四个页面，分别是待处理用户、流量渠道、投保渠道、SEM 文章配置。

![](https://gitlab.com/linhe/qtao-cloud-service/-/raw/master/qtao/marketing.png)

##### 流量渠道

- [页面地址](https://testfns.guanghebao.com/#/nav/marketing/channelManage)
- 角色：运营

所谓流量渠道即指公司运营获取客户的渠道，目前我们主要通过 SEM、公众号投放和合作方获取客户。SEM 指的是搜索引擎营销，通过客户在搜索引擎（如百度）上的关键字匹配营销软文。公众号投放除了公司内部公众号“光合保”的资讯产出外，还包括其他相关站点（潜在客户可能浏览的公众号资讯）的投放，这两种方式一般最后都引流到渠道落地页让客户预约来获取客户信息。而合作方提供的客户一般给到的就是客户信息，对应到系统中就不在`市场运营`模块了，一般来说，公司是在[预约池](https://testfns.guanghebao.com/#/nav/insurancePlanning/appointmentList)中直接创建预约来引入客户。

回到系统的`流量渠道`，这里记录的就是公司所有发布的渠道信息。按渠道状态分可分为已上线和已下线渠道，按渠道级别分可分为一级流量渠道和二级流量渠道（还有一种分类方式可分为公司分配、自有客户、客户转介绍、其他）。在该页面可进行渠道的增改查功能，目前没实现删除功能，废弃的渠道可操作`下线`让渠道不可用。

##### 投保渠道

- [页面地址](https://testfns.guanghebao.com/#/nav/marketing/purchaseChannelList)
- 角色：运营

流量渠道表示的是客户从哪里来，而投保渠道表示的是要把客户引到哪里去。毕竟作为一家类经纪公司，最终的目的是为客户定制符合客户预算和需求的保险，再让客户在保险公司投保。

也就是说，投保渠道所列是各经纪公司信息。同样，投保渠道也有增改查功能，暂时没实现删除功能。

##### 待处理用户

- [页面地址](https://testfns.guanghebao.com/#/nav/marketing/pendingUsers)
- 角色：？

待处理用户统计的是用户在渠道落地页提交的信息。通过上面关于流量渠道和投保渠道的介绍，我相信理解这样的流程并不难，当然我也画了一个简单的关于流程扭转的示意图：

![](https://gitlab.com/linhe/qtao-cloud-service/-/raw/master/qtao/state-reverse.png)

> 蓝色表示按钮，椭圆表示状态，粉色表示渠道，绿色是预约池，灰色框表示操作人，黑色框表示各系统或页面。

实际上，`待处理用户`页面不仅仅包含状态为待处理的用户，它囊括了待处理、处理中、已作废和已预约状态的用户。可以从上图中清晰的看出各状态下的转变可能。所有状态都可以直接转变为`已预约`，`已预约`状态的用户都会被同步到`预约池`（这是后话）。

##### SEM 文章配置

- [页面地址](https://testfns.guanghebao.com/#/nav/marketing/semArticleConfig)
- 角色：运营

前面已经介绍过 SEM 了，它的翻译为搜索引擎营销。那么，`SEM 文章配置`页所提供的也就是搜索引擎上的文章配置。它分为`文章列表`和`文章默认配置`两个 Tab，文章默认配置也就是文章共有部分的配置，像公众号名称信息、引导用户的 banner、页脚链接等实际上不需要重复编写的一些内容。系统中，它当然是可以编辑的，点击`编辑`按钮进入编辑。

### 保险规划

`保险规划`可能是系统业务最多的一个模块，也是最主要的模块。它对应的角色为保险规划师，主要分为首页、我的日程表、我的预约、我的成单和管理等页面，其中`管理`还有更细分的页面。

##### 我的日程表

- [页面地址](https://testfns.guanghebao.com/#/nav/insurancePlanning/mySchedule)
- 角色：规划师

日程表不难理解，该页面就是以小时为单位标注规划师的日程安排，规划师免去了做笔记的烦恼，什么时间约什么客户，以及客户什么样的背景资料都一目了然。当然，如果规划师有自己的日程安排也可以创建个人日程，这就区分了`客户日程`和`个人日程`，客户日程与客户 ID 绑定，个人日程与客户就没什么关系了，自然就是不关联客户 ID 的。提到这一点是因为从[日程底层数据](https://testfns.guanghebao.com/#/nav/system/scheduleData)中可以很直观的看出来————个人日程没有客户 ID。

##### 管理-员工日程

- [页面地址](https://testfns.guanghebao.com/#/nav/insurancePlanning/staffSchedule)
- 角色：？

单单有对应运营的日程表是不够的，例如公司要开周会或者主持培训，为了避免客服在规划师没空的时候分配客户，每个规划师不得不分别创建个人日程。这样的操作繁琐而且不必要，这时`员工日程`的`创建多人日程`就发挥它的作用了。`创建多人日程`可同时为多人创建日程并同步到规划师日程安排中。`员工日程`分为`员工日程`和`多人日程管理`两个 Tab，员工日程显示某一日员工的日程安排，既然规划师才有日程安排，自然这里`员工`指的就是规划师。

多人日程管理负责增删改查。

##### 管理-预约池

- [页面地址](https://testfns.guanghebao.com/#/nav/insurancePlanning/appointmentList)
- 角色：？

前面提到过，已预约的用户会被同步到`预约池`，也就是这里提到的页面。从流程扭转图中不难看出，此时客服已与客户进行了初步的沟通，这时候客服也许已经根据各规划师的日程安排分配了规划师，也许还没有。如果没有，就需要在`预约池`中分配规划师。所以`预约池`中可针对单条用户信息`分配`规划师、修`改渠道`信息、`编辑约定 FNA 时间`等。当然前面还提到了`流量渠道`中还包括合作方的客户信息，我们可以在此页面`创建预约`将客户信息保存。另外，`预约池`还有`导出`表格、`删除`单条用户信息等功能按钮。

按常理来说，`预约池`应该只包含未分配状态下的用户。实际上，它还包含待 FNA、待一促、待二促、待投保、已投保和无效等状态的用户。

##### 管理-保险产品

- [页面地址](https://testfns.guanghebao.com/#/nav/insurancePlanning/productManage)
- 角色：运营

保险产品顾名思义就是由保险公司开发的产品，这个页面会列出保险产品的详细信息，主要功能是增删改查。目前公司销售的保险产品主要分为四大类，它们分别是寿险、重疾险、医疗险和意外险，接下来可能还会接入年金险等险种。

##### 我的预约

- [页面地址](https://testfns.guanghebao.com/#/nav/insurancePlanning/myAppointmentList)
- 角色：运营

`我的预约`指的是当前登录规划师名下所有状态下的客户信息，包括待 FNA、待一促、待二促、待投保、已投保等。规划师可在当前页面增改查用户的预约信息。其中增表示`创建预约`，与`预约池`不同，规划师无权添加渠道客户资料，但可添加`亲友单`和`客户转介绍`单，这两类单由于在成本上不同，因此佣金计算方式也不相同。

规划师在跟进客户的过程中，比如客服首次沟通后确定了客户 FNA 时间，此时用户为待 FNA 状态，规划师在`我的预约`中点击用户 ID 可打开 ALC。一般来说，待 FNA 状态的用户我们除了知道客户姓名和电话外，其他一无所知，因此，规划师需要在 FNA 过程了解客户的家庭成员分布`添加家庭成员`并`保存家庭成员`，以及了解客户的`家庭财务`资料，如年收入、支出以此来制定保险方案，了解清楚后`保存财务`信息。在完成 FNA 之前，规划师还需要确定`客户优先级`、填写`跟进记录`、`创建客户日程`，然后就可以扭转状态到待一促了。

![](https://gitlab.com/linhe/qtao-cloud-service/-/raw/master/qtao/alc.png)

在待一促状态显示其他 3 个 Tab —— `需求总结`、`定制方案`、`投保订单`，也就是说 FNA 后就可以为客户定制方案了。`需求总结`是可选的，`定制方案`中需要注意在`添加方案`并开启对客户显示后，还要在方案中选择保险产品和年投保金额才是一个正常流程的方案，否则方案的投保金额为 0。方案制定完成就可`分享方案书`的二维码给客户了，方案书可引导客户投保。

##### 管理-投保订单库

- [页面地址](https://testfns.guanghebao.com/#/nav/insurancePlanning/insuranceOrderList)
- 角色：？

一旦客户投保成功，公司专人可进入保险产品提供方的系统获取订单信息，再在`投保订单库`点击`批量上传`来同步信息。保单记录会统计投保记录也会统计退保记录，每个保单对应一名规划师，管理员可`更改规划师`，当然也可以删改查订单信息。

##### 我的成单

- [页面地址](https://testfns.guanghebao.com/#/nav/insurancePlanning/myInsuranceOrderList)
- 角色：运营

属于规划师的客户投保信息会同步到`我的订单`，被规划师`确认订单`后可记入规划师业绩。 


### 产品

![](https://gitlab.com/linhe/qtao-cloud-service/-/raw/master/qtao/nav.png)

##### 用户管理

- [页面地址](https://testfns.guanghebao.com/#/nav/index/userManage)
- 角色：产品

所有用户信息，跟预约池类似。

##### 微信授权记录

- [页面地址](https://testfns.guanghebao.com/#/nav/index/wechatBoundLog)
- 角色：产品

用户在小程序上，授权手机号时，记录其手机号与微信id。

用户可换手机号授权，此情况会有一个微信id的多个手机号授权记录（先解除原有的，再记录新的）。

### 名词解释

| 名词 | 解释 |
| :---: | :--- |
| 投保订单 |  |
| 标保（标准保费） |  |
| 交接客户 | 之前有其他规划师服务过的客户，现交接给新规划师进行跟进。 |




